#region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Outfile=emailtest.exe
#AutoIt3Wrapper_UseX64=n
#AutoIt3Wrapper_Res_Fileversion=1.0.0.0
#AutoIt3Wrapper_Res_LegalCopyright=Copyright 2012, Navcom Defense Electronics. All rights reserved.
#AutoIt3Wrapper_Res_Language=1033
#AutoIt3Wrapper_Add_Constants=n
#AutoIt3Wrapper_Run_Tidy=y
#endregion ;**** Directives created by AutoIt3Wrapper_GUI ****

#comments-start
	Email test script, written with AutoIT3
	Copyright 2012, Navcom Defense Electronics
	All Rights Reserved
	Written by Sean Cox, scox@navcom.com
	
	Version 1.00 2012/01/27 07:49 PDST
	
	1.00 Initial file setup
	
#comments-end

;includes
#include <Array.au3>
#include-once
#include <Date.au3>
;#include <INet.au3>

;Script Options Below
Opt("MustDeclareVars", 1) ;0=no, 1=require pre-declare
Opt("TrayIconDebug", 1) ;0=no info, 1=debug line info
Opt("TrayIconHide", 0) ;0=show, 1=hide tray icon

;Variable Declarations
Const $sConfigFile = @ScriptDir & "\backup.ini" ;Configuration file path
Const $iEffDate = @YEAR & @MON & @MDAY ;Backup effective date, in YYYMMDD format
Const $sLogFile = @ScriptDir & "\test.log"

Global $sBackupType, $s7zip, $asVolumes[1], $sType, $sArg

;User Defined Functions
Func _Error($sMessage, $iSeverity = 3)
	Local $h, $sLevel
	Local $asLevels[1]
	$asLevels = StringSplit("Fatal|Alert|Critical|Error|Warning|Notification|Informational|Debug", "|")
	If $iSeverity = 0 Then
		$sLevel = "Super Fatal"
	Else
		$sLevel = $asLevels[$iSeverity]
	EndIf
	$h = FileOpen($sLogFile, 9)
	If $h = -1 Then
		MsgBox(0, "Error", "Unable to open log file: " & $sLogFile)
		Exit
	EndIf
	FileWriteLine($h, @MON & "/" & @MDAY & "/" & @YEAR & " " & @HOUR & ":" & @MIN & " " & $sLevel & ":" & $sMessage & @CRLF)
	FileClose($h)
	IniWrite($sConfigFile, "main", "status", $sLevel)
	If $iSeverity = 0 Then Exit ;Error severity 0 is Super Fatal, only used by _Exit. Prevents loop if cannot send email.
	If $iSeverity <= 3 Then _Exit()
EndFunc   ;==>_Error

Func _Exit()
	Local $i = 1, $as[1], $sSMTPserver, $sFromName, $sFromAddress, $sToAddress
	$as[0] = "Error log as follows:"
	While @error > -1
		_ArrayAdd($as, FileReadLine($sLogFile, $i))
		If $as[$i] = "" Then ExitLoop
		$i = $i + 1
	WEnd
	$sSMTPserver = IniRead($sConfigFile, "email", "SMTPserver", "127.0.0.1")
	$sFromName = IniRead($sConfigFile, "email", "FromName", "Backup7")
	$sFromAddress = IniRead($sConfigFile, "email", "FromAddress", "administrator@backup.7")
	$sToAddress = IniRead($sConfigFile, "email", "ToAddress", "Administrator")
	MsgBox(0, "Debug", "$sSMTPserver: ." & $sSMTPserver & ".")
	If _INetSmtpMail($sSMTPserver, $sFromName, $sFromAddress, $sToAddress, "Backup encountered issues. Please review log.", $as, @ComputerName, -1, 1) = 0 Then
		_Error("Could not send email. Error code was: " & @error, 0)
	EndIf
	Exit
EndFunc   ;==>_Exit

;===============================================================================
;
; Function Name:    _INetSmtpMail()
; Description:      Sends an email using SMTP over TCP IP.
; Parameter(s):     $s_SmtpServer	- SMTP server to be used for sending email
;                   $s_FromName		- Name of sender
;                   $s_FromAddress	- eMail address of sender
;                   $s_ToAddress	- Address that email is to be sent to
;                   $s_Subject		- Subject of eMail
;					$as_Body		- Single dimension array containing the body of eMail as strings
;					$s_helo			- Helo identifier (default @COMPUTERNAME) sometime needed by smtp server
;					$s_first		- send before Helo identifier (default @CRLF) sometime needed by smtp server
;					$b_trace		- trace on a splash window (default 0 = no trace)
; Requirement(s):   None
; Return Value(s):  On Success - Returns 1
;                   On Failure - 0  and sets
;											@ERROR = 1		-	Invalid Parameters
;											@ERROR = 2		-	Unable to start TCP
;											@ERROR = 3		-	Unable to resolve IP
;											@ERROR = 4		-	Unable to create socket
;											@ERROR = 5x		-	Cannot open SMTP session
;											@ERROR = 50x	-	Cannot send body
;											@ERROR = 5000	-	Cannot close SMTP session
; Authors:        Original function to send email via TCP 	- Asimzameer
;					Conversion to UDF						- Walkabout
;					Correction	Helo, timeout, trace		- Jpm
;					Correction send before Helo				- Jpm
;
;===============================================================================
Func _INetSmtpMail($s_SmtpServer, $s_FromName, $s_FromAddress, $s_ToAddress, $s_Subject = "", $as_Body = "", $s_helo = "", $s_first=" ", $b_trace = 0)

	Local $v_Socket
	Local $s_IPAddress
	Local $i_Count
	Local $s_Send[6]
	Local $s_ReplyCode[6];Return code from SMTP server indicating success

	If $s_SmtpServer = "" Or $s_FromAddress = "" Or $s_ToAddress = "" Or $s_FromName = "" Or StringLen($s_FromName) > 256 Then
		SetError(1)
		Return 0
	EndIf
	If $s_helo = "" Then $s_helo = @ComputerName
	If TCPStartup() = 0 Then
		SetError(2)
		Return 0
	EndIf
	StringRegExp($s_SmtpServer, "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)")
	If @extended Then
		$s_IPAddress = $s_SmtpServer
		MsgBox(0, "Debug", "$s_IPAddress: " & $s_IPAddress)
	Else
		$s_IPAddress = TCPNameToIP($s_SmtpServer)
		MsgBox(0, "Debug", "Resolved $s_IPAddress: " & $s_IPAddress)
	EndIf
	If $s_IPAddress = "" Then
		TCPShutdown()
		SetError(3)
		Return 0
	EndIf
	MsgBox(0, "Debug", "$s_IPAddress: " & $s_IPAddress)
	$v_Socket = TCPConnect($s_IPAddress, 25)
	If $v_Socket = -1 Then
		TCPShutdown()
		SetError(4)
		Return (0)
	EndIf

	$s_Send[0] = "HELO " & $s_helo & @CRLF
	If StringLeft($s_helo,5) = "EHLO " Then $s_Send[0] = $s_helo & @CRLF
	$s_ReplyCode[0] = "250"

	$s_Send[1] = "MAIL FROM: <" & $s_FromAddress & ">" & @CRLF
	$s_ReplyCode[1] = "250"
	$s_Send[2] = "RCPT TO: <" & $s_ToAddress & ">" & @CRLF
	$s_ReplyCode[2] = "250"
	$s_Send[3] = "DATA" & @CRLF
	$s_ReplyCode[3] = "354"
	
	Local $aResult = _Date_Time_GetTimeZoneInformation()
	Local $bias = -$aResult[1]/60
	Local $biasH = Int($bias)
	Local $biasM = 0
	If $biasH <> $bias Then $biasM =  Abs($bias - $biasH) * 60
	$bias =  StringFormat(" (%+.2d%.2d)", $biasH, $biasM)

	$s_Send[4] = 	"From:" & $s_FromName & "<" & $s_FromAddress & ">" & @CRLF & _
			"To:" & "<" & $s_ToAddress & ">" & @CRLF & _
			"Subject:" & $s_Subject & @CRLF & _
			"Mime-Version: 1.0" & @CRLF & _
			"Date: " & _DateDayOfWeek(@WDAY, 1) & ", " & @MDAY & " " & _DateToMonth(@MON, 1) & " " & @YEAR & " " & @HOUR & ":" & @MIN & ":" & @SEC & $bias & @CRLF & _
			"Content-Type: text/plain; charset=US-ASCII" & @CRLF & _
			@CRLF
	$s_ReplyCode[4] = ""

	$s_Send[5] = @CRLF & "." & @CRLF
	$s_ReplyCode[5] = "250"

	; open stmp session
	If _SmtpSend($v_Socket, $s_Send[0], $s_ReplyCode[0], $b_trace, "220", $s_first) Then
		SetError(50)
		Return 0
	EndIf
	; send header
	For $i_Count = 1 To UBound($s_Send) - 2
		If _SmtpSend($v_Socket, $s_Send[$i_Count], $s_ReplyCode[$i_Count], $b_trace) Then
			SetError(50 + $i_Count)
			Return 0
		EndIf
	Next

	; send body records (a record can be multiline : take care of a subline beginning with a dot should be ..)
	For $i_Count = 0 To UBound($as_Body) - 1
		; correct line beginning with a dot
		If StringLeft($as_Body[$i_Count], 1) = "." Then $as_Body[$i_Count] = "." & $as_Body[$i_Count]

		If _SmtpSend($v_Socket, $as_Body[$i_Count] & @CRLF, "", $b_trace) Then
			SetError(500 + $i_Count)
			Return 0
		EndIf
	Next

	; close the smtp session
	$i_Count = UBound($s_Send) - 1
	If _SmtpSend($v_Socket, $s_Send[$i_Count], $s_ReplyCode[$i_Count], $b_trace) Then
		SetError(5000)
		Return 0
	EndIf

	TCPCloseSocket($v_Socket)
	TCPShutdown()
	Return 1
EndFunc   ;==>_INetSmtpMail

; internals routines----------------------------------
Func _SmtpTrace($str, $timeout = 0)
	Local $W_TITLE = "SMTP trace"
	Local $g_smtptrace = ControlGetText($W_TITLE, "", "Static1")
	$str = StringLeft(StringReplace($str, @CRLF, ""), 70)
	$g_smtptrace &= @HOUR & ":" & @MIN & ":" & @SEC & " " & $str & @LF
	If WinExists($W_TITLE) Then
		ControlSetText($W_TITLE, "", "Static1", $g_smtptrace)
	Else
		SplashTextOn($W_TITLE, $g_smtptrace, 400, 500, 500, 100, 4 + 16, "", 8)
	EndIf
	If $timeout Then Sleep($timeout * 1000)
EndFunc   ;==>_SmtpTrace

Func _SmtpSend($v_Socket, $s_Send, $s_ReplyCode, $b_trace, $s_IntReply="", $s_first="")
    Local $s_Receive, $i, $timer
    If $b_trace Then _SmtpTrace($s_Send)

    If $s_IntReply <> ""  Then

        ; Send special first char to awake smtp server
        If $s_first <> -1 Then
            If TCPSend($v_Socket, $s_first) = 0 Then
                TCPCloseSocket($v_Socket)
                TCPShutdown()
                Return 1; cannot send
            EndIf
        EndIf

        ; Check intermediate reply before HELO acceptation
        $s_Receive = ""
        $timer = TimerInit()
        While StringLeft($s_Receive,StringLen($s_IntReply)) <> $s_IntReply And TimerDiff($timer) < 45000
            $s_Receive = TCPRecv($v_Socket, 1000)
            If $b_trace And $s_Receive <> "" Then _SmtpTrace("intermediate->" & $s_Receive)
        WEnd
    EndIf

    ; Send string.
    If TCPSend($v_Socket, $s_Send) = 0 Then
        TCPCloseSocket($v_Socket)
        TCPShutdown()
        Return 1; cannot send
    EndIf

    $timer = TimerInit()

    $s_Receive = ""
    While $s_Receive = "" And TimerDiff($timer) < 45000
        $i += 1
        $s_Receive = TCPRecv($v_Socket, 1000)
        If $s_ReplyCode = "" Then ExitLoop
    WEnd

    If $s_ReplyCode <> "" Then
        ; Check replycode
        If $b_trace Then _SmtpTrace($i & " <- " & $s_Receive)

        If StringLeft($s_Receive, StringLen($s_ReplyCode)) <> $s_ReplyCode Then
            TCPCloseSocket($v_Socket)
            TCPShutdown()
            If $b_trace Then _SmtpTrace("<-> " & $s_ReplyCode, 5)
            Return 2; bad receive code
        EndIf
    EndIf

    Return 0
EndFunc   ;==>_SmtpSend


_Exit()
