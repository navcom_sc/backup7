#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Outfile=backup.exe
#AutoIt3Wrapper_Res_Description=7-zip Backup
#AutoIt3Wrapper_Res_Fileversion=1.12.0.0
#AutoIt3Wrapper_Res_LegalCopyright=Copyright 2015, Navcom Defense Electronics, Inc. All rights reserved.
#AutoIt3Wrapper_Res_Language=1033
#AutoIt3Wrapper_Res_requestedExecutionLevel=asInvoker
#AutoIt3Wrapper_Run_Tidy=y
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#comments-start
	Backup script, written with AutoIT3
	Copyright 2015, Navcom Defense Electronics
	All Rights Reserved
	Written by Sean Cox, scox@navcom.com

	Use Git to see log now. Version 1.10.2 2015/02/26 12:27 PDST

	0.01 Initial file setup
	0.02 subMain and subError added
	0.03 Converted to AutoIT3
	0.04 Subs converted to Functions. Added _FatalError, _GetStatus, _SetStatus, _SetType
	0.05 Added _GetVolumes, _GetDestination, _BackupVolume (unfinished).
	Temporarily changed _FatalError to clone output to MsgBox for debugging.
	0.06 Finished _BackupVolume. Added _GetArguments. Changes _SetType to use single character codes. (Makes file naming simpler.)
	0.07 Added _GetSet, _UpdateSet functions.
	0.08 Added _ReplaceSet, _Sweeper, _Cleanup functions.
	0.09 Changed all references to incremental to differential, including changing $sType value from "i" to "d" as this more accurately reflects the type of backup.
	1.00 Added retention levels to INI file vs. hard-coded.
	1.01 _FatalError renamed _Error, now supports multiple error levels.
	1.02 _Exit added, in progress.
	1.03 _Exit done.
	1.04 Will check to see if it is the only running copy. Log is emailed if errors occur. Program can be in any folder, INI file is searched in the same folder.
	1.05 Fixed setting lastfull (was _UpdateSet, is now _ReplaceSet). Updated _GetArguments to allow for per volume arguments.
	1.06 Fixed error in _Error
	1.07 Fixed Lastfull not being populated (_Replace set must be passed an array as the value to write!), Added a Force option in the ini file to allow
	forcing a type of backup (dwmqy).
	1.08 Added ini option to enable trace window during log email. _INetSmtpMail() doesn't seem to like using IP address pulled from ini...
	1.09 Removed _ArraySort of the volumes, allowing manual ordering. Added _PreRun and _PostRun
	1.10 Fixed an issue than when a cleanup deletion failed, a while loop repeated until crash

	ToDo:
	Fix _PreRun: error codes and running net commands.
	Add reporting options: size of backups, time taken. (Number of files/directories?)
	Change _GetSet to allow for optional sections.
	Add option to clean logs with backup sets.
	Need to change from a volume mentality to a backup set mentaility.
	Add an on-first-run detection so there is a full backup, and no fail on no lastfull arguments.

	Long-term ToDo:
	If no ini file, pop-up asking to create template and exit.
	Figure out logging to windows event log.
	Use 7zip DLL calls.
	Figure out 7-zip error vs status output. (7-zip doesn't separate error level ouputs.?!)
	Write GUI screens for progress/configuration.

#comments-end

;includes
#include <Array.au3>
#include <INet.au3>
#include <File.au3>

;Script Options Below
Opt("MustDeclareVars", 1) ;0=no, 1=require pre-declare
Opt("TrayIconDebug", 1) ;0=no info, 1=debug line info
Opt("TrayIconHide", 0) ;0=show, 1=hide tray icon

;Variable Declarations
Const $sConfigFile = @ScriptDir & "\backup.ini" ;Configuration file path
Const $iEffDate = @YEAR & @MON & @MDAY ;Backup effective date, in YYYMMDD format
Const $sLogFile = @ScriptDir & "\" & $iEffDate & ".log"

Global $sBackupType, $s7zip, $asVolumes[1], $sType, $sArg, $iMaxError = 9

;User Defined Functions
Func _Error($sMessage, $iSeverity = 3)
	Local $h, $sLevel
	Local $asLevels[1]
	If $iSeverity < $iMaxError Then $iMaxError = $iSeverity ;Keep track of how bad it has been
	$asLevels = StringSplit("Fatal|Alert|Critical|Error|Warning|Notification|Informational|Debug", "|")
	If $iSeverity = 0 Then
		$sLevel = "Super Fatal"
	Else
		$sLevel = $asLevels[$iSeverity]
	EndIf
	$h = FileOpen($sLogFile, 9)
	If $h = -1 Then
		MsgBox(0, "Error", "Unable to open log file: " & $sLogFile)
		Exit
	EndIf
	FileWriteLine($h, @MON & "/" & @MDAY & "/" & @YEAR & " " & @HOUR & ":" & @MIN & " " & $sLevel & ":" & $sMessage & @CRLF)
	FileClose($h)
	IniWrite($sConfigFile, "main", "status", $sLevel)
	If $iSeverity = 0 Then Exit ;Error severity 0 is Super Fatal, only used by _Exit. Prevents loop if cannot send email.
	If $iSeverity <= 3 Then _Exit()
EndFunc   ;==>_Error

Func _GetStatus($sWhen, $sExpected)
	Local $s
	If FileExists($sConfigFile) Then
		$s = IniRead($sConfigFile, "main", "status", "fatal")
		If $s <> $sExpected Then
			_Error($sWhen & " status check failed. Status read was: " & $s & ". Expected status was: " & $sExpected, 6)
		EndIf
	Else
		_Error("Backup configuration file not found. Path checked was: " & $sConfigFile)
	EndIf
	Return $s
EndFunc   ;==>_GetStatus

Func _SetStatus($sWhen, $sNewStatus)
	If FileExists($sConfigFile) Then
		IniWrite($sConfigFile, "main", "status", $sNewStatus)
		If @error <> 0 Then
			_Error($sWhen & " status update failed. Attempting to set status to: " & $sNewStatus)
		EndIf
	Else
		_Error("Backup configuration file not found. Path checked was: " & $sConfigFile)
	EndIf
EndFunc   ;==>_SetStatus

Func _SetType()
	Local $s, $i
	If IniRead($sConfigFile, "main", "force", "") = "" Then
		If @WDAY <> 1 Then ;Is today not Sunday?
			$s = "d" ;Daily differential
			Return $s
		Else
			If @MDAY > 7 Then ;Is it not the first week of the month?
				$s = "w" ;Weekly full
				Return $s
			Else
				For $i = 1 To 10 Step 3 ;Is it a "Quarterly" month?
					If @MON = $i Then
						$s = "q" ;Quarterly full
						Return $s
					EndIf
				Next
				$s = "m" ;Monthly full
				Return $s
			EndIf
		EndIf
	Else
		$s = IniRead($sConfigFile, "main", "force", "full")
		Return $s
	EndIf
EndFunc   ;==>_SetType

Func _GetVolumes()
	Local $as[1], $s
	$as = IniReadSectionNames($sConfigFile)
	_ArrayDelete($as, 0)
	For $s In $as
		If StringLen($s) = 1 Then _ArrayAdd($asVolumes, $s)
	Next
	;_ArraySort($asVolumes) ;Not sure why I did this originally, but disabled now. Able to define order in config file.
	_ArrayDelete($asVolumes, 0) ;First item is always a null, get rid of it.
EndFunc   ;==>_GetVolumes

Func _GetDestination()
	Local $s
	If FileExists($sConfigFile) Then
		$s = IniRead($sConfigFile, "main", "destination", "missing")
		If $s == "missing" Then
			_Error("Backup destination missing from configuration file: " & $sConfigFile)
		EndIf
		If StringRight($s, 1) <> "\" Then $s = $s & "\"
	Else
		_Error("Backup configuration file not found. Path checked was: " & $sConfigFile)
	EndIf
	If FileExists($s) Then
		Return $s
	Else
		_Error("Backup destination not found. Path checked was: " & $s)
	EndIf
EndFunc   ;==>_GetDestination

Func _GetArguments($sType, $sVolume)
	Local $s
	$s = IniRead($sConfigFile, "main", "7z", "error")
	If $sType == "d" Then
		$s = $s & " u " & _GetDestination() & IniRead($sConfigFile, "main", "lastfull", "error") & $sVolume & ".7z "
	Else
		$s = $s & " u " & _GetDestination() & $iEffDate & $sType & $sVolume & ".7z "
	EndIf
	If IniRead($sConfigFile, $sVolume, "overridearg", "error") == "error" Then
		$s = $s & " " & IniRead($sConfigFile, "main", "defaultarg", "error")
	Else
		$s = $s & " " & IniRead($sConfigFile, $sVolume, "overridearg", "error")
	EndIf
	For $s2 In _GetSet("globalexclude")
		If $s2 <> "" Then $s = $s & " -xr!" & $s2
	Next
	For $s2 In _GetSet("exclude", $sVolume)
		If $s2 <> "" Then $s = $s & " -xr!" & $s2
	Next
	If $sType == "d" Then $s = $s & " " & IniRead($sConfigFile, "main", "differentialarg", "error") & _GetDestination() & $iEffDate & $sType & $sVolume & ".7z"
	$s = $s & " " & IniRead($sConfigFile, $sVolume, "include", "error")
	; 7z ip doesn't send error messages to STDERR. WTF? $s = $s & " 2>" & $sLogFile
	$s = $s & " >>temp.log"
	If StringInStr($s, "error") > 0 Then _Error("Could not retrive arguments for " & $sVolume & " drive from " & $sConfigFile & ". Argument string is: " & $s)
	Return $s
EndFunc   ;==>_GetArguments

Func _BackupVolume($sVolume)
	_SetStatus("Backup", "Running: Backing up " & $sVolume)
	Local $iReturn
	If FileExists(_GetDestination() & $iEffDate & $sType & $sVolume & ".7z") Then
		_Error("Backup file already exists! " & _GetDestination() & $iEffDate & $sType & $sVolume & ".7z", 4)
	Else
		If $sType == "d" Then
			If FileExists(_GetDestination() & IniRead($sConfigFile, "main", "lastfull", "error") & $sVolume & ".7z") == 0 Then
				_Error("The last full backup file is missing! " & _GetDestination() & IniRead($sConfigFile, "main", "lastfull", "error") & $sVolume & ".7z", 4)
				Return
			EndIf
		EndIf
		$iReturn = RunWait(_GetArguments($sType, $sVolume), "c:\", @SW_MINIMIZE)
		If @error <> 0 Then _Error("Could not run 7zip. Command line used was: " & _GetArguments($sType, $sVolume), 4)
		If $iReturn == 1 Then
			_Error("7-zip reported non-fatal errors while backing up volume " & $sVolume, 5)
		ElseIf $iReturn > 1 Then
			_Error("7zip reported error: " & $iReturn & ". Command line used was: " & _GetArguments($sType, $sVolume), 4)
		EndIf

	EndIf
EndFunc   ;==>_BackupVolume

Func _GetSet($sSetName, $sSection = "main")
	Local $asSet[1]
	$asSet = StringSplit(IniRead($sConfigFile, $sSection, $sSetName, "error"), "|")
	_ArrayDelete($asSet, 0)
	If _ArraySearch($asSet, "error") <> -1 Then _Error("Error while fetching set " & $sSection & ":" & $sSetName & ". Could not read set.")
	Return $asSet
EndFunc   ;==>_GetSet

Func _UpdateSet($sSetName, $sNewItem, $sSection = "main")
	Local $asSet[1]
	$asSet = _GetSet($sSetName, $sSection)
	If _ArraySearch($asSet, $sNewItem) <> -1 Then
		_Error("Error while updating set " & $sSection & ":" & $sSetName & ". Attempted to add duplicate item: " & $sNewItem, 4)
	Else
		_ArrayAdd($asSet, $sNewItem)
	EndIf
	If $asSet[0] == "" Then _ArrayDelete($asSet, 0)
	If IniWrite($sConfigFile, $sSection, $sSetName, _ArrayToString($asSet)) <> 1 Then
		_Error("Error while updating set " & $sSection & ":" & $sSetName & ". Could not update set.")
	EndIf
EndFunc   ;==>_UpdateSet

Func _ReplaceSet($sSetName, $asSet, $sSection = "main")
	If IniWrite($sConfigFile, $sSection, $sSetName, _ArrayToString($asSet)) <> 1 Then

		_Error("Error while replacing set " & $sSection & ":" & $sSetName & ". Could not replace set.")
	EndIf
EndFunc   ;==>_ReplaceSet

Func _Sweeper($sDelSet, $sDelType, $iDelRetain, $sDelVol)
	_SetStatus("InProgress", "Running: " & $sDelSet & " sweeping drive " & $sDelVol)
	Local $as[1]
	$as = _GetSet($sDelSet)
	While UBound($as) > $iDelRetain
		If FileDelete(_GetDestination() & $as[0] & $sDelType & $sDelVol & ".7z") <> 1 Then
			_Error("Could not delete old daily backup file " & _GetDestination() & $as[0] & $sDelType & $sDelVol & ".7z. Error is: " & @error, 4)
			_ArrayDelete($as, 0) ;If you do not delete the item you just tried to delete from the array it will try to delete it repetedly. Like 11 Million times repeatedly.
		Else
			_ArrayDelete($as, 0)
			_ReplaceSet($sDelSet, $as)
		EndIf
	WEnd
EndFunc   ;==>_Sweeper

Func _Cleanup()
	_SetStatus("InProgress", "Running: Cleaning")
	Local $as[1]
	$as = _GetSet("retentionlevels")
	If UBound($as) <> 3 Then
		_Error("Retention levels missing or invalid.", 4)
		Return
	Else
		For $s In $asVolumes
			If $sType == "w" Then
				_Sweeper("differentialset", "d", $as[0], $s)
			ElseIf $sType == "m" Then
				_Sweeper("weeklyset", "w", $as[1], $s)
			ElseIf $sType == "q" Then
				_Sweeper("monthlyset", "m", $as[2], $s)
			EndIf
		Next
	EndIf
EndFunc   ;==>_Cleanup

Func _Exit()
	Local $i = 1, $as[1], $sSMTPserver, $sFromName, $sFromAddress, $sToAddress, $iTrace
	$as[0] = "Error log as follows:"
	If FileExists($sLogFile) Then
		While @error > -1
			_ArrayAdd($as, FileReadLine($sLogFile, $i))
			If $as[$i] = "" Then ExitLoop
			$i = $i + 1
		WEnd
		$sSMTPserver = IniRead($sConfigFile, "email", "SMTPserver", "127.0.0.1")
		$sFromName = IniRead($sConfigFile, "email", "FromName", "Backup7")
		$sFromAddress = IniRead($sConfigFile, "email", "FromAddress", "administrator@backup.7")
		$sToAddress = IniRead($sConfigFile, "email", "ToAddress", "Administrator")
		$iTrace = IniRead($sConfigFile, "email", "Trace", 0)
		If _INetSmtpMail($sSMTPserver, $sFromName, $sFromAddress, $sToAddress, "Backup encountered issues. Please review log.", $as, @ComputerName, -1, $iTrace) = 0 Then
			_Error("Could not send email. Error code was: " & @error, 0)
		EndIf
	EndIf
	Exit
EndFunc   ;==>_Exit

Func _PreRun($sVolume, $sType = "pre")
	Local $as[1], $sExec, $sParms, $iReturn
	_SetStatus($sType & "run: " & $sVolume, $sType & "run: " & $sVolume)
	$as = _GetSet($sType & "run", $sVolume)
	For $s In $as
		If $s = "" Then ContinueLoop ;If no pre/postrun set, don't try to run it
		$sExec = ""
		$sParms = ""
		If StringRegExp($s, "\A\x22") Then ;Look for a quote (") at the start of the string, allowing for spaces in path/executable name.
			If StringLen($s) <> StringInStr($s, '"', 0, 2) Then ;Look for characters after the quotes, separate out executable and parameters
				$sExec = StringLeft($s, StringInStr($s, '"', 0, 2))
				$sParms = StringRight($s, StringLen($s) - StringInStr($s, '"', 0, 2) - 1)
			EndIf
		ElseIf StringInStr($s, " ") Then ;Look for spaces, separate out executable and parameters
			$sExec = StringLeft($s, StringInStr($s, " ") - 1)
			$sParms = StringRight($s, StringLen($s) - StringInStr($s, " "))
		Else
			$sExec = $s
		EndIf
		$iReturn = ShellExecuteWait($sExec, $sParms)
		If @error <> 0 Then _Error("Could not execute " & $sType & "run command for volume " & $sVolume & ". Passed string was: " & $sExec & "|" & $sParms & " Error code was: " & @error, 4)
		If $iReturn == 0 Then _Error("Could not execute " & $sType & "run command for volume " & $sVolume & ". Passed string was: " & $sExec & "|" & $sParms & " Return code was: " & $iReturn, 4)
	Next
EndFunc   ;==>_PreRun

Func _PostRun($sVolume) ;Just call _PreRun with type set to "post"
	_PreRun($sVolume, "post")
EndFunc   ;==>_PostRun

Func _ProcessLog() ;For capturing 7-zip log output.
	Local $i = 1, $as[1]
	If FileExists("temp.log") Then
		_FileReadToArray("temp.log", $as)
		For $i = $as[0] To 1 Step -1
			If $as[$i] = "" Then
				_ArrayDelete($as, $i)
			ElseIf StringInStr($as[$i], "7-Zip ") Then
				Sleep(1);do something?
			EndIf
		Next
	EndIf
EndFunc   ;==>_ProcessLog


Func _Main()
	_GetStatus("Initial", "success")
	If StringInStr(_GetStatus("Startup", "success"), "Running") Then _Error("Job already or still running. Aborting!")
	_SetStatus("Startup", "Running")
	; Effective date is set to Const $iEffDate
	$sType = _SetType()
	_PreRun("main")
	_GetVolumes()
	For $s In $asVolumes
		_PreRun($s)
		_BackupVolume($s)
		_PostRun($s)
		_ProcessLog()
	Next
	If $iMaxError > 4 Then
		If $sType <> "d" Then
			Local $as[1]
			$as[0] = $iEffDate & $sType
			_ReplaceSet("lastfull", $as); _ReplaceSet was passing an array, not a string
		EndIf
		If $sType == "w" Then _UpdateSet("weeklyset", $iEffDate)
		If $sType == "m" Then _UpdateSet("monthlyset", $iEffDate)
		If $sType == "q" Then _UpdateSet("quarterlyset", $iEffDate)
		If $sType == "d" Then _UpdateSet("differentialset", $iEffDate)
		_Cleanup()
		_PostRun("main")
		_SetStatus("Done", "success")
	Else
		_SetStatus("Done", "error")
	EndIf
EndFunc   ;==>_Main

;Do it already!
_Main()
_Exit()




